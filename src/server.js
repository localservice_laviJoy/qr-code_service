const express = require("express");
const morgan = require("morgan");
require("dotenv").config();
const cors = require("cors");

// router
const testRouter = require("./testRout/test.route");

// init class
class Server {
  constructor() {
    this.server = express();
    this.PORT = process.env.PORT || 1717;
    this.HOST = process.env.HOST;
  }

  async start() {
    this.initMiddlewares();
    this.startListening();
    this.initRoutes();
    this.handleErrors();
  }

  initMiddlewares() {
    this.server.use(cors());
    this.server.use(morgan("dev"));
    this.server.use(express.json());
    this.server.use(this.handleErrors);
  }

  initRoutes() {
    this.server.use("/api", testRouter);
  }

  startListening() {
    this.server.listen(this.PORT, () =>
      console.log(`Server is running on ${this.HOST}:${this.PORT}`)
    );
  }

  handleErrors(error, req, res, next) {
    if (error) {
      console.log("Error Code", error.code);
      console.log("Error message", error.message);
    }
  }
}

module.exports = Server;
